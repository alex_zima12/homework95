import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Cocktails from "./components/Cocktails/Cocktails";
import NewCocktail from "./containers/NewCocktail/NewCocktail";
import Cocktail from "./components/Cocktail/Cocktail";
import MyCocktails from "./components/MyCocktails/MyCocktails";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo} />;
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/"
                   exact
                   component={Cocktails}/>
            <ProtectedRoute
                isAllowed={user}
                path="/cocktails/new"
                exact
                component={NewCocktail}
            />
            <ProtectedRoute
                isAllowed={user}
                path="/cocktails/my_cocktails"
                exact
                component={MyCocktails}
            />
            <Route
                path="/cocktails/:id"
                component={Cocktail}/>
        </Switch>
    );
};

export default Routes;