import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktail} from "../../store/actions/CocktailsActions";
import defaultImage from "../../assets/images/image_not_available.png";
import {apiURL} from "../../constants";

const Cocktail = (props) => {
    const dispatch = useDispatch();
    const id = props.match.params.id
    const cocktail = useSelector(state => state.cocktails.cocktail);

    useEffect(() => {
        dispatch(fetchCocktail(id));
    }, [dispatch,id]);

    let cardImage = defaultImage;
    if (cocktail.image) {
        cardImage =  apiURL + "/uploads/" + cocktail.image;
    }

    return cocktail && (
        <div className="container">
            <h1>{cocktail.name}</h1>
            <img src={cardImage} className="card-img-top flex-md-grow-1 " alt="cocktail"/>
            <h2>Recipe:</h2>
            <p>{cocktail.recipe}</p>
            <ul>
            {cocktail.ingredients.map(ing => {
                return <li><b>{ing.title}: </b> {ing.amount}</li>
            })}
            </ul>
        </div>
    );
};

export default Cocktail;