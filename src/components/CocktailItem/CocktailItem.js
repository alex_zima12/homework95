import React from 'react';
import {apiURL} from "../../constants";
import {NavLink} from 'react-router-dom';
import defaultImage from "../../assets/images/image_not_available.png";
import {useDispatch, useSelector} from "react-redux";
import {publishCocktail, deleteCocktail} from "../../store/actions/CocktailsActions";

const myStyle = {
    width: "300px",
    height:"300px"
};

const CocktailItem = ({name, image, id}) => {
    const user = useSelector(state => state.users.user);
    const isPublished = useSelector(state => state.cocktails.isPublished);


    const dispatch = useDispatch();

    const  publishItem = () => {
        dispatch(publishCocktail(id));
    };

    const  deleteItem = () => {
        dispatch(deleteCocktail(id));
    };

    let cardImage = defaultImage;
    if (image) {
        cardImage =  apiURL + "/uploads/" + image;
    }

    return (
        <div className="col-md-4">
        <div className="card d-flex col-sm m-3 p-3">
            <h5 className="card-title  flex-md-grow-1 ">{name}</h5>
            <img src={cardImage}
                 className="card-img-top flex-md-grow-1 p-3"
                 alt="cocktail"
                 style={myStyle}
            />
            <div className="card-body d-flex flex-column">
                <NavLink to={"/cocktails/" + id} className="btn btn-primary"> See moreeeeeeeeee</NavLink>
            </div>
            {user && user.user.role === "admin" ?
                <>
                    {isPublished === false || user.user.isPublished === false  ?
                        <> <p className="text-danger text-center border"> The item is not published</p>
                            <button onClick={publishItem}> Publish</button>
                        </> :
                        null
                    }
                    <button className="text-danger mt-3" onClick={deleteItem}> Delete</button>
                </>
                : null
            }
        </div>
        </div>
    );
};

export default CocktailItem;