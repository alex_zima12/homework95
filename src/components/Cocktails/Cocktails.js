import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchCocktails} from "../../store/actions/CocktailsActions";
import CocktailItem from "../CocktailItem/CocktailItem";

const Cocktails = () => {
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);


    useEffect(() => {
        dispatch(fetchCocktails());
    }, [dispatch]);

    return (
        <div className="container">
            <div className="row">
                {cocktails.map(cocktail => {
                    return <CocktailItem
                        key={cocktail._id}
                        id={cocktail._id}
                        name={cocktail.name}
                        image={cocktail.image}
                        isPublished={cocktail.isPublished}
                    />
                })}
        </div>
        </div>
    );
};

export default Cocktails;