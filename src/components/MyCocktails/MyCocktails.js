import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchMyCocktails} from "../../store/actions/CocktailsActions";
import CocktailItem from "../CocktailItem/CocktailItem";

const MyCocktails = () => {

    const user = useSelector(state => state.users.user);
    const myCocktails = useSelector(state => state.cocktails.myCocktails);
    const dispatch = useDispatch();
    const id = user.user._id


    useEffect(() => {
        dispatch(fetchMyCocktails(id));
    }, [dispatch,id]);

    return (

            <div className="container">
                <div className="row">
                    {myCocktails.map(cocktail => {
                        return <CocktailItem
                            key={cocktail._id}
                            id={cocktail._id}
                            name={cocktail.name}
                            // recipe={cocktail.recipe}
                            image={cocktail.image}
                            isPublished={cocktail.isPublished}
                        />
                    })}
                </div>
            </div>

    );
};

export default MyCocktails;