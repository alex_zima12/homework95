import React from "react";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/UsersActions";
import defaultAvatar from "../../../assets/images/image_not_available.png";

const myStyle = {
    width: "50px",
    height:"50px"
};

const UserMenu = ({user}) => {
     const dispatch = useDispatch();

    const logout = () => {
        dispatch(logoutUser());
    };

    return user && (
        <>
            <div>
                <NavLink className="font-weight-bold text-light m-3" to={"/cocktails/new"}>Add New Cocktail</NavLink>
                <NavLink className="font-weight-bold text-light m-3" to={"/cocktails/my_cocktails"}>My Cocktails</NavLink>
            </div>
            <div>
                <div className="font-weight-bold text-light ">Hello, {user.user.user.displayName}!
                    <img src={user.user.user.avatar ? user.user.user.avatar : defaultAvatar }
                         className="rounded circle rounded-circle m-3"
                         style={myStyle}
                         alt="user"
                    />
                </div>
                <button className="font-weight-bold" onClick={logout}>Logout</button>
            </div>
        </>
    );
};

export default UserMenu;