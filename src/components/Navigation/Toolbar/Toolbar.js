import React from 'react';
import {NavLink} from 'react-router-dom';
import FacebookLogin from "../../FacebookLogin/FacebookLogin";
import UserMenu from "../Menu/UserMenu";

const Toolbar = (user) => {

    return (
        <>
            <nav className="navbar navbar-dark bg-primary row">
                <NavLink className="font-weight-bold text-light text-decoration-none display-4 ml-3" to={"/"}>
                    Cocktail Catalog App
                </NavLink>
            </nav>
            <div className="navbar navbar-dark bg-primary row">
                <div className="container">
                    {user.user == null ?
                        <div className="mt-3">
                            <FacebookLogin/>
                        </div>
                        :
                        <UserMenu user = {user}/>
                    }
                </div>
            </div>
        </>
    );
};

export default Toolbar;