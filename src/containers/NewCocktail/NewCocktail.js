import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {createNewCocktail} from "../../store/actions/CocktailsActions";

const NewCocktail = (props) => {
    const dispatch = useDispatch();

    const [state, setState] = useState({
        name: '',
        recipe: '',
        image: '',
        ingredients: [
            {title: '',
             amount: ''},
        ]
    });

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        Object.keys(state).forEach(key => {
            if ( Array.isArray(state[key]))
            {
                state[key] = JSON.stringify(state[key]);
            }
            formData.append(key, state[key]);

        });

        dispatch(createNewCocktail(formData)).then(() => {
            props.history.push("/")})
    }


    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    }
    const inputChangeHandlerIngr = (e, ind) => {
        const name = e.target.name;
        const value = e.target.value;
        const ingredientCopy = {...state.ingredients[ind]};
        ingredientCopy[name] = value;
        const stateCopyIngredients = [...state.ingredients]
        stateCopyIngredients[ind] = ingredientCopy;
        setState({
            ...state,
            ingredients: [...stateCopyIngredients]
        })
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    const addIngredient = () => {
        setState({
            ingredients: [
                ...state.ingredients,
                {title: '', amount: ''}
            ]
        });
    };

    const removeIngredient = ind => {
        const ingredients = [...state.ingredients];
        ingredients.splice(ind, 1);
        setState({ingredients});
    };

    return (
        <div className="container bg-gradient-info">
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Cocktail name</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           placeholder="Enter cocktail name"
                           value={state.name}
                           onChange={inputChangeHandler}
                           name="name"
                           required/>
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Recipe</span>
                    </div>
                    <textarea
                           className="form-control"
                           placeholder="Write this cocktail recipe"
                           value={state.recipe}
                           onChange={inputChangeHandler}
                           name="recipe"
                           required/>
                </div>
                {state.ingredients.map((ingr, ind) => (
                        <div>
                                <input
                                    key={ind}
                                    name="title"
                                    placeholder="Ingredient name:"
                                    type="text"
                                    className="p-1"
                                    value={state.ingredients[ind].title}
                                    onChange={(e) => inputChangeHandlerIngr(e,ind)}
                                />
                                <input
                                    key={ind+2}
                                    name="amount"
                                    placeholder="Amount:"
                                    type="text"
                                    className="m-2 p-1"
                                    value={state.ingredients[ind].amount}
                                    onChange={(e) => inputChangeHandlerIngr(e,ind)}
                                />
                                <button
                                    key={ind+3}
                                    type="button"
                                    className="btn btn-danger"
                                    onClick={() => removeIngredient(ind)}>
                                    x</button>
                        </div>
                    ))
                }

                <button type="button"
                        className="mb-3"
                        onClick={addIngredient}
                >Add ingredient</button>

                <div className="input-group mb-3 ">
                    <label htmlFor="image">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        id="image-album"
                        onChange={fileChangeHandler}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                    id="btn-album"
                >Add new cocktail
                </button>
            </form>
        </div>
    );
};

export default NewCocktail;