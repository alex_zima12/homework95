import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from "connected-react-router";
import axiosApi from "./axiosApi";
import 'bootstrap/dist/css/bootstrap.min.css';
import store, {history} from "./store/configureStore";
import App from "./App";

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.user.token;
    } catch(e) {
        // do nothing, no token exists
    }
    return config;
});

const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.hydrate(app, document.getElementById('root'));