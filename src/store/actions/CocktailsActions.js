import {
    CREATE_COCKTAIL_SUCCESS,
    CREATE_COCKTAIL_ERROR,
    FETCH_COCKTAILS_SUCCESS,
    FETCH_COCKTAIL_SUCCESS,
    DELETE_COCKTAIL_SUCCESS,
    PUBLISH_COCKTAIL_SUCCESS,
    FETCH_MY_COCKTAILS_SUCCESS
} from "./actionTypes";
import axios from "../../axiosApi";
import {push} from "connected-react-router";

const createNewCocktailError = (error) => {
    return {type: CREATE_COCKTAIL_ERROR, error}
};

export const createNewCocktail = data => {
    return async dispatch => {
        try {
            await axios.post('/cocktails', data);
            dispatch({type: CREATE_COCKTAIL_SUCCESS});
            alert("ваш коктейль находится на рассмотрении модератора");
            dispatch(push("/"));
        } catch (e) {
            dispatch(createNewCocktailError(e.response.data.error));
        }
    };
}

const fetchCocktailsSuccess = cocktails => {
    return {type: FETCH_COCKTAILS_SUCCESS, cocktails};
};

export const fetchCocktails = () => {
    return async dispatch => {
        try {
            await axios.get("/cocktails").then(response => {
            dispatch(fetchCocktailsSuccess(response.data));
        });
        } catch (e) {
            console.log(e);
        }
    };
};


const fetchCocktailSuccess = cocktail => {
    return {type: FETCH_COCKTAIL_SUCCESS, cocktail};
};

export const fetchCocktail = (id) => {
    return async dispatch => {
        try {
        await axios.get("/cocktails/" + id).then(response => {
            dispatch(fetchCocktailSuccess(response.data));
        });
        } catch (e) {
            console.log(e);
        }
    };
};

const deleteCocktailSuccess = (id) => {
    return {type: DELETE_COCKTAIL_SUCCESS, id}
};

export const deleteCocktail = id => {
    return async dispatch => {
        try {
            const response = await axios.delete('/cocktails/' + id);
            alert(response.data.message)
            dispatch(deleteCocktailSuccess(id));
        } catch (e) {
            console.log(e);
        }
    };
}

export const publishCocktail = id => {
    return async dispatch => {
        try {
            await axios.put('/cocktails/' + id + '/publish');
            dispatch({type: PUBLISH_COCKTAIL_SUCCESS});
        } catch (e) {
            console.log(e);
        }
    };
}

const fetchMyCocktailsSuccess = cocktails => {
    return {type: FETCH_MY_COCKTAILS_SUCCESS, cocktails};
};

export const fetchMyCocktails = (id) => {
    return  async dispatch => {
        try {
        await axios.get("/cocktails?user=" + id).then(response => {
            dispatch(fetchMyCocktailsSuccess(response.data));
        });
        } catch (e) {
            console.log(e);
        }
    };
};

