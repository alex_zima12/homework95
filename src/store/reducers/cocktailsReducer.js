import {FETCH_COCKTAILS_SUCCESS,
    FETCH_COCKTAIL_SUCCESS,
    DELETE_COCKTAIL_SUCCESS,
    PUBLISH_COCKTAIL_SUCCESS,
    FETCH_MY_COCKTAILS_SUCCESS

} from "../actions/actionTypes";

const initialState = {
    cocktails: [],
    myCocktails: [],
    cocktail: '',
    isPublished: false
};

const cocktailsReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails};
        case FETCH_MY_COCKTAILS_SUCCESS:
            return {...state, myCocktails: action.cocktails};
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktail: action.cocktail};
        case DELETE_COCKTAIL_SUCCESS:
            const newCocktails = [...state.cocktails];
            const id = newCocktails.findIndex(p => p.id === action.id);
            newCocktails.splice(id, 1);
            return {...state, cocktails : [...newCocktails]};
        case PUBLISH_COCKTAIL_SUCCESS:
            return {...state, isPublished: true};
        default:
            return state;
    }
};

export default cocktailsReducer;